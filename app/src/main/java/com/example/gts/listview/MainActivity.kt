package com.example.gts.listview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.gts.listview.R.id.rv_country_list
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    // Initializing an empty ArrayList to be filled with animals
    val country: ArrayList<String> = ArrayList()
    val flags = arrayOf(R.drawable.chinaflag,R.drawable.jordanflag,R.drawable.flagofuae,R.drawable.southkorea)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Loads country into the ArrayList
        addCountry()

        // Creates a vertical Layout Manager
        rv_country_list.layoutManager = LinearLayoutManager(this)
        rv_country_list.adapter = CountryAdapter(country,flags, this)

    }

    // Adds animals to the empty animals ArrayList
    fun addCountry() {
        country.add("China")
        country.add("Jordan")
        country.add("UAE")
        country.add("South Korea")
    }
}
